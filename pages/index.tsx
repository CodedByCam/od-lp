import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { useState } from "react";
import { FaClipboardList, FaUsers, FaEnvelope, FaGlobe } from "react-icons/fa";

const Home: NextPage = () => {
  const [email, setEmail] = useState<string>("");
  return (
    <div className="container">
      <Head>
        <title>On Deck</title>
        <meta name="description" content="On Deck Web Application" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">
        <div className="top-container">
          <div className="header-container">
            <div className="header-logo">
              <Image
                alt="logo image"
                src="/vercel.svg"
                width={40}
                height={40}
              />
              <p>On Deck</p>
            </div>

            <button className="btn-round">Refer a friend</button>
          </div>

          <div className="top-landing">
            <h1>Discover emerging tech talent</h1>
            <p>
              Chat with engineers, designers, operators, and more in minutes. 
            </p>
            <div className="landing-input-container">
              <div>
                <FaEnvelope />
                <input
                  className="landing-input"
                  placeholder="Enter your email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <button className="request-access-btn">Request Access</button>
            </div>
          </div>
        </div>

        <div className="drops-container">
          <FaClipboardList /> Upcoming drops
          <div className="drops-box-container">
            <div className="dbc-header">
              <Image alt="h-image" src="/vercel.svg" width={40} height={40} />
              <span>Q2 On Deck Fellows</span>
            </div>

            <div className="dc-tags-container">
              <div className="dc-tag">
                <FaUsers />
                <span>12 People</span>
              </div>

              <div className="dc-tag">
                <FaGlobe />
                <span>Remote</span>
              </div>
            </div>

            <p>
              Ft. a product designer at Facebook, a FAANG team lear, an ex-Uber
              growth designer and other top product designers.
            </p>
          </div>
          <div className="drops-box-container">
            <div className="dbc-header">
              <Image alt="h-image" src="/vercel.svg" width={40} height={40} />
              <span>Remote Product Designers</span>
            </div>

            <div className="dc-tags-container">
              <div className="dc-tag">
                <FaUsers />
                <span>12 People</span>
              </div>

              <div className="dc-tag">
                <FaGlobe />
                <span>Remote</span>
              </div>
            </div>

            <p>
              Ft. a product designer at Facebook, a FAANG team lear, an ex-Uber
              growth designer and other top product designers.
            </p>
          </div>
          <div className="drops-box-container">
            <div className="dbc-header">
              <Image alt="h-image" src="/vercel.svg" width={40} height={40} />
              <span>Remote Full Stack Software Engineers</span>
            </div>

            <div className="dc-tags-container">
              <div className="dc-tag">
                <FaUsers />
                <span>12 People</span>
              </div>

              <div className="dc-tag">
                <FaGlobe />
                <span>Remote</span>
              </div>
            </div>

            <p>
              Ft. a product designer at Facebook, a FAANG team lear, an ex-Uber
              growth designer and other top product designers.
            </p>
          </div>
          <div className="drops-box-container">
            <div className="dbc-header">
              <Image alt="h-image" src="/vercel.svg" width={40} height={40} />
              <span>Startup Recruiters</span>
            </div>

            <div className="dc-tags-container">
              <div className="dc-tag">
                <FaUsers />
                <span>12 People</span>
              </div>

              <div className="dc-tag">
                <FaGlobe />
                <span>Remote</span>
              </div>
            </div>

            <p>
              Ft. a product designer at Facebook, a FAANG team lear, an ex-Uber
              growth designer and other top product designers.
            </p>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Home;
